package me.vinceh121.lstp;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import org.ldaptive.LdapEntry;
import org.ldaptive.SearchResponse;
import org.ldaptive.io.LdifReader;
import org.ldaptive.schema.AttributeType;
import org.ldaptive.schema.ObjectClass;
import org.ldaptive.schema.Schema;
import org.ldaptive.schema.SchemaFactory;

import me.vinceh121.lstp.model.Clazz;
import me.vinceh121.lstp.model.CodeGenerator;
import me.vinceh121.lstp.model.Field;

public class Lstp {
	private final SearchResponse res;

	public static void main(final String[] args) throws IOException {
		final LdifReader read = new LdifReader(new FileReader(args[0]));
		final SearchResponse res = read.read();
		final Lstp lstp = new Lstp(res);
		final Collection<Clazz> model = lstp.generateClassModel();

		final File outDir = new File("./out");
		outDir.mkdir();
		final CodeGenerator codeGen = new CodeGenerator();
		codeGen.setPkg("me.vinceh121.lstptest");
		codeGen.generateClasses(model, outDir);

	}

	public Lstp(final SearchResponse res) {
		this.res = res;
	}

	public Collection<Clazz> generateClassModel() {
		final Collection<Clazz> model = new Vector<>();
		for (final LdapEntry e : this.res.getEntries()) {
			final Schema sch = SchemaFactory.createSchema(e);
			for (final ObjectClass objClazz : sch.getObjectClasses()) {
				final Clazz clazz = new Clazz();
				clazz.setName(StringUtils.capitalize(objClazz.getName()));
				if (objClazz.getOptionalAttributes() != null) {
					for (final String opt : objClazz.getOptionalAttributes()) {
						final Field f = this.makeType(sch, opt);
						f.setOptional(true);
						clazz.add(f);
					}
				}
				model.add(clazz);
			}
		}
		return model;
	}

	public Field makeType(final Schema sch, final String attr) {
		final Field field = new Field();
		field.setName(attr);

		final AttributeType type = sch.getAttributeType(attr);
		if (type != null) {
			if (type.getSuperiorType() != null) {
				field.setType(Lstp.getJavaTypeFromSuperiorType(type.getSuperiorType()));
			} else if (sch.getMatchingRule(type.getOID()) != null) {
				System.out.println(sch.getMatchingRule(type.getOID()) + "\n");
			}

			if (type.getDescription() != null) {
				field.setComment(attr);
			}
		}

		if (field.getType() == null) {
			field.setType("String");
		}

		return field;
	}

	public static String getJavaTypeFromSuperiorType(final String supType) {
		switch (supType) {
		case "name":
		case "labeledURI":
		case "distinguishedName":
		case "postalAddress":
		case "telephoneNumber":
		case "mail":
			return "String";
		}
		return null;
	}
}
