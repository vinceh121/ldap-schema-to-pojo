package me.vinceh121.lstp.model;

public class Field {
	private String name, type, comment;
	private boolean optional, list, deprecated;

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public String getComment() {
		return this.comment;
	}

	public void setComment(final String comment) {
		this.comment = comment;
	}

	public boolean isOptional() {
		return this.optional;
	}

	public void setOptional(final boolean optional) {
		this.optional = optional;
	}

	public boolean isList() {
		return this.list;
	}

	public void setList(final boolean list) {
		this.list = list;
	}

	public boolean isDeprecated() {
		return this.deprecated;
	}

	public void setDeprecated(final boolean deprecated) {
		this.deprecated = deprecated;
	}

	@Override
	public String toString() {
		return "Field [name="
				+ this.name
				+ ", type="
				+ this.type
				+ ", comment="
				+ this.comment
				+ ", optional="
				+ this.optional
				+ ", list="
				+ this.list
				+ "]";
	}
}
