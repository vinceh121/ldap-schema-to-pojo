package me.vinceh121.lstp.model;

import java.util.HashMap;
import java.util.Map;

public class Clazz {
	private String name;
	private Map<String, Field> fields = new HashMap<>();

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public Map<String, Field> getFields() {
		return this.fields;
	}

	public void setFields(final Map<String, Field> fields) {
		this.fields = fields;
	}

	public Field add(final Field value) {
		return this.fields.put(value.getName(), value);
	}

	public Field get(final String key) {
		return this.fields.get(key);
	}

	public Field put(final String key, final Field value) {
		return this.fields.put(key, value);
	}

	public Field remove(final Object key) {
		return this.fields.remove(key);
	}

	@Override
	public String toString() {
		return "Clazz [name=" + this.name + ", fields=" + this.fields + "]";
	}
}
