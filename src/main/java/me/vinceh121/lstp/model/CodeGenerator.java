package me.vinceh121.lstp.model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import me.vinceh121.lstp.StringUtils;

public class CodeGenerator {
	private String pkg, indent = "\t";
	private boolean gettersSetters = true;

	public CodeGenerator() {
	}

	public void generateClasses(final Collection<Clazz> model, final File outDir) throws IOException {
		Objects.requireNonNull(model);
		Objects.requireNonNull(outDir);
		if (!outDir.isDirectory()) {
			throw new IllegalArgumentException("outDir must be a folder");
		}

		for (final Clazz c : model) {
			final File javaFile = outDir.toPath().resolve(c.getName() + ".java").toFile();
			final FileWriter writer = new FileWriter(javaFile);
			this.writeClassFile(c, writer);
			writer.close();
		}
	}

	public void writeClassFile(final Clazz c, final Writer writer) throws IOException {
		if (this.pkg != null) {
			writer.write("package ");
			writer.write(this.pkg);
			writer.write(";\n\n");
		}

		writer.write("public class ");
		writer.write(c.getName());
		writer.write(" {\n");
		this.writeFields(c.getFields(), writer);
		writer.write("}\n");
	}

	public void writeFields(final Map<String, Field> fields, final Writer writer) throws IOException {
		if (this.gettersSetters) {
			this.writeFieldsSettersGetters(fields, writer);
		} else {
			this.writeFieldsPublic(fields, writer);
		}
	}

	private void writeFieldsPublic(final Map<String, Field> fields, final Writer writer) throws IOException {
		for (final Field f : fields.values()) {
			if (f.getComment() != null) {
				this.writeFieldJavadoc(f, writer);
			}
			this.indent(writer);
			writer.write("public ");
			writer.write(f.getType());
			writer.write(" ");
			writer.write(f.getName());
			writer.write(";\n");
		}
		writer.write("\n");
	}

	private void writeFieldsSettersGetters(final Map<String, Field> fields, final Writer writer) throws IOException {
		// FIELDS
		for (final Field f : fields.values()) {
			this.indent(writer);
			writer.write("private ");
			writer.write(f.getType());
			writer.write(" ");
			writer.write(f.getName());
			writer.write(";\n");
		}
		writer.write("\n\n");

		// GETTERS & SETTERS
		for (final Field f : fields.values()) {
			if (f.getComment() != null) {
				this.writeGetterJavadoc(f, writer);
			}
			this.indent(writer);
			writer.write("public ");
			writer.write(f.getType());
			writer.write(" ");
			writer.write(StringUtils.makeSetGetName("get", f.getName()));
			writer.write("() {\n");
			this.indent(writer);
			this.indent(writer);
			writer.write("return this.");
			writer.write(f.getName());
			writer.write(";\n");
			this.indent(writer);
			writer.write("}\n\n");

			if (f.getComment() != null) {
				this.writeSetterJavadoc(f, writer);
			}
			this.indent(writer);
			writer.write("public void ");
			writer.write(StringUtils.makeSetGetName("set", f.getName()));
			writer.write("(");
			writer.write(f.getType());
			writer.write(" ");
			writer.write(f.getName());
			writer.write(") {\n");
			this.indent(writer);
			this.indent(writer);
			writer.write("this.");
			writer.write(f.getName());
			writer.write(" = ");
			writer.write(f.getName());
			writer.write(";\n");
			this.indent(writer);
			writer.write("}\n\n");
		}
	}

	private void writeFieldJavadoc(final Field f, final Writer writer) throws IOException {
		this.indent(writer);
		writer.write("/**\n");
		this.indent(writer);
		writer.write(" * ");
		writer.write(f.getComment());
		writer.write("\n");
		this.indent(writer);
		writer.write(" */");
	}

	private void writeGetterJavadoc(final Field f, final Writer writer) throws IOException {
		this.indent(writer);
		writer.write("/**\n");
		this.indent(writer);
		writer.write(" * ");
		writer.write(f.getComment());
		writer.write("\n");
		this.indent(writer);
		writer.write(" * \n");
		this.indent(writer);
		writer.write(" * @return ");
		writer.write(f.getName());
		writer.write(" The ");
		writer.write(f.getName());
		writer.write("\n");
		this.indent(writer);
		writer.write(" */\n");
	}

	private void writeSetterJavadoc(final Field f, final Writer writer) throws IOException {
		this.indent(writer);
		writer.write("/**\n");
		this.indent(writer);
		writer.write(" * ");
		writer.write(f.getComment());
		writer.write("\n");
		this.indent(writer);
		writer.write(" * \n");
		this.indent(writer);
		writer.write(" * @param ");
		writer.write(f.getName());
		writer.write(" The ");
		writer.write(f.getName());
		writer.write(" to set\n");
		this.indent(writer);
		writer.write(" */\n");
	}

	private void indent(final Writer writer) throws IOException {
		writer.write(this.indent);
	}

	public String getPkg() {
		return this.pkg;
	}

	public void setPkg(final String pkg) {
		this.pkg = pkg;
	}

	public String getIndent() {
		return this.indent;
	}

	public void setIndent(final String indent) {
		this.indent = indent;
	}

	public boolean isGettersSetters() {
		return this.gettersSetters;
	}

	public void setGettersSetters(final boolean gettersSetters) {
		this.gettersSetters = gettersSetters;
	}

	public static boolean isPrimitive(final String type) {
		switch (type) {
		case "boolean":
		case "byte":
		case "char":
		case "short":
		case "int":
		case "long":
		case "float":
		case "double":
			return true;
		}
		return false;
	}
}
