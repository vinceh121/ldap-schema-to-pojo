package me.vinceh121.lstp;

public final class StringUtils {

	public static String makeSetGetName(final String prefix, final String field) {
		return prefix + StringUtils.capitalize(field);
	}

	public static String capitalize(final String str) {
		final StringBuilder sb = new StringBuilder();
		sb.append(Character.toUpperCase(str.charAt(0)));
		sb.append(str.substring(1));
		return sb.toString();
	}

}
